use regex::{Match, Regex};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::error::LexerError;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum TokenClass {
    Keyword,
    BuiltinType,
    Literal,
    Parentheses,
    Separator,
    Operator,
    Identifier,
    Ignorable,
    NotApplicable,
}

impl TokenClass {
    fn for_type(tt: &TokenType) -> Self {
        match tt {
            TokenType::Comment => TokenClass::Ignorable,
            TokenType::Newline => TokenClass::Ignorable,
            TokenType::Whitespace => TokenClass::Ignorable,
            TokenType::KeywordAction => TokenClass::Keyword,
            TokenType::KeywordIf => TokenClass::Keyword,
            TokenType::KeywordElse => TokenClass::Keyword,
            TokenType::KeywordElseif => TokenClass::Keyword,
            TokenType::KeywordRepeat => TokenClass::Keyword,
            TokenType::KeywordUntil => TokenClass::Keyword,
            TokenType::KeywordWhile => TokenClass::Keyword,
            TokenType::KeywordEnd => TokenClass::Keyword,
            TokenType::KeywordType => TokenClass::Keyword,
            TokenType::KeywordReturns => TokenClass::Keyword,
            TokenType::KeywordReturn => TokenClass::Keyword,
            TokenType::KeywordSelf => TokenClass::Keyword,
            TokenType::TypeBool => TokenClass::BuiltinType,
            TokenType::TypeInt => TokenClass::BuiltinType,
            TokenType::TypeFloat => TokenClass::BuiltinType,
            TokenType::TypeText => TokenClass::BuiltinType,
            TokenType::Bool => TokenClass::Literal,
            TokenType::FloatingPoint => TokenClass::Literal,
            TokenType::Integral => TokenClass::Literal,
            TokenType::Text => TokenClass::Literal,
            TokenType::Identifier => TokenClass::Identifier,
            TokenType::LeftParens => TokenClass::Parentheses,
            TokenType::RightParens => TokenClass::Parentheses,
            TokenType::OperatorAssign => TokenClass::Operator,
            TokenType::OperatorConcat => TokenClass::Operator,
            TokenType::OperatorPlus => TokenClass::Operator,
            TokenType::OperatorMinus => TokenClass::Operator,
            TokenType::OperatorMul => TokenClass::Operator,
            TokenType::OperatorDiv => TokenClass::Operator,
            TokenType::OperatorSmallerOrEqual => TokenClass::Operator,
            TokenType::OperatorSmaller => TokenClass::Operator,
            TokenType::OperatorBiggerOrEqual => TokenClass::Operator,
            TokenType::OperatorBigger => TokenClass::Operator,
            TokenType::SeparatorPoint => TokenClass::Separator,
            TokenType::SeparatorColon => TokenClass::Separator,
            TokenType::SeparatorComma => TokenClass::Separator,
            TokenType::EOF => TokenClass::NotApplicable,
            TokenType::NotYetLexed => TokenClass::NotApplicable,
        }
    }
}

#[derive(Clone, Copy, PartialEq, EnumIter, Debug)]
pub enum TokenType {
    // Comment
    Comment,
    // Newline, to allow for counting lines (together with Comment)
    Newline,
    // Whitespace
    Whitespace,
    // Keywords
    KeywordAction,
    KeywordIf,
    KeywordElse,
    KeywordElseif,
    KeywordRepeat,
    KeywordUntil,
    KeywordWhile,
    KeywordEnd,
    KeywordType,
    KeywordReturns,
    KeywordReturn,
    KeywordSelf,
    // Type names
    TypeBool,
    TypeInt,
    TypeFloat,
    TypeText,
    // Literal values
    Bool,
    FloatingPoint,
    Integral,
    Text,
    // Identifier
    Identifier,
    // Parentheses
    LeftParens,
    RightParens,
    // Operators
    OperatorAssign,
    OperatorConcat,
    OperatorPlus,
    OperatorMinus,
    OperatorMul,
    OperatorDiv,
    OperatorSmallerOrEqual,
    OperatorSmaller,
    OperatorBiggerOrEqual,
    OperatorBigger,
    // Separators
    SeparatorPoint,
    SeparatorColon,
    SeparatorComma,
    EOF,
    NotYetLexed,
}

impl TokenType {
    pub fn regex(&self) -> &'static Regex {
        match self {
            TokenType::Comment => &RE_COMMENT,
            TokenType::Whitespace => &RE_WHITESPACE,
            TokenType::Newline => &RE_NEWLINE,
            TokenType::KeywordAction => &RE_EMPTY,
            TokenType::KeywordIf => &RE_EMPTY,
            TokenType::KeywordElse => &RE_EMPTY,
            TokenType::KeywordElseif => &RE_EMPTY,
            TokenType::KeywordRepeat => &RE_EMPTY,
            TokenType::KeywordUntil => &RE_EMPTY,
            TokenType::KeywordWhile => &RE_EMPTY,
            TokenType::KeywordEnd => &RE_EMPTY,
            TokenType::KeywordType => &RE_EMPTY,
            TokenType::KeywordReturns => &RE_EMPTY,
            TokenType::KeywordReturn => &RE_EMPTY,
            TokenType::KeywordSelf => &RE_EMPTY,
            TokenType::TypeBool => &RE_TYPE_BOOL,
            TokenType::TypeInt => &RE_TYPE_INT,
            TokenType::TypeFloat => &RE_TYPE_FLOAT,
            TokenType::TypeText => &RE_TYPE_TEXT,
            TokenType::Bool => &RE_LIT_BOOL,
            TokenType::FloatingPoint => &RE_LIT_FLOAT,
            TokenType::Integral => &RE_LIT_INT,
            TokenType::Text => &RE_LIT_TEXT,
            TokenType::Identifier => &RE_IDENTIFIER,
            TokenType::LeftParens => &RE_PAR_LEFT,
            TokenType::RightParens => &RE_PAR_RIGHT,
            TokenType::OperatorAssign => &RE_OP_ASSIGN,
            TokenType::OperatorConcat => &RE_OP_CONCAT,
            TokenType::OperatorPlus => &RE_OP_PLUS,
            TokenType::OperatorMinus => &RE_OP_MINUS,
            TokenType::OperatorMul => &RE_OP_MUL,
            TokenType::OperatorDiv => &RE_OP_DIV,
            TokenType::OperatorSmallerOrEqual => &RE_OP_SMALLER_OR_EQ,
            TokenType::OperatorSmaller => &RE_OP_SMALLER,
            TokenType::OperatorBiggerOrEqual => &RE_OP_BIGGER_OR_EQ,
            TokenType::OperatorBigger => &RE_OP_BIGGER,
            TokenType::SeparatorPoint => &RE_SEP_POINT,
            TokenType::SeparatorColon => &RE_SEP_COLON,
            TokenType::SeparatorComma => &RE_SEP_COMMA,
            TokenType::EOF => &RE_EMPTY,
            TokenType::NotYetLexed => &RE_EMPTY,
        }
    }
}

lazy_static! {
    // Type names
    static ref RE_TYPE_BOOL: Regex = Regex::new(r"^boolean").unwrap();
    static ref RE_TYPE_INT: Regex = Regex::new(r"^integer").unwrap();
    static ref RE_TYPE_FLOAT: Regex = Regex::new(r"^number").unwrap();
    static ref RE_TYPE_TEXT: Regex = Regex::new(r"^text").unwrap();
    // Literal values
    static ref RE_LIT_BOOL: Regex = Regex::new(r"^(true|false)").unwrap();
    static ref RE_LIT_FLOAT: Regex = Regex::new(r"^-?\d*\.\d+").unwrap();
    static ref RE_LIT_INT: Regex = Regex::new(r"^(?:(0b[01][01_]*)|(0x[\da-fA-F][\da-fA-F_]*)|(-?\d[\d_]*))").unwrap();
    static ref RE_LIT_TEXT: Regex = Regex::new(r#"^"[\pL\pN\pS\pP[:blank:]]*""#).unwrap();
    // Identifier
    static ref RE_IDENTIFIER: Regex = Regex::new(r"^[\w--[0-9]]\w*").unwrap();
    // Parenthesis
    static ref RE_PAR_LEFT: Regex = Regex::new(r#"^\("#).unwrap();
    static ref RE_PAR_RIGHT: Regex = Regex::new(r#"^\)"#).unwrap();
    // Operators
    static ref RE_OP_ASSIGN: Regex = Regex::new(r"^=").unwrap();
    static ref RE_OP_CONCAT: Regex = Regex::new(r"^\+\+").unwrap();
    static ref RE_OP_PLUS: Regex = Regex::new(r"^\+").unwrap();
    static ref RE_OP_MINUS: Regex = Regex::new(r"^\-").unwrap();
    static ref RE_OP_MUL: Regex = Regex::new(r"^\*").unwrap();
    static ref RE_OP_DIV: Regex = Regex::new(r"^/").unwrap();
    static ref RE_OP_SMALLER: Regex = Regex::new(r"^<").unwrap();
    static ref RE_OP_SMALLER_OR_EQ: Regex = Regex::new(r"^<=").unwrap();
    static ref RE_OP_BIGGER: Regex = Regex::new(r"^>").unwrap();
    static ref RE_OP_BIGGER_OR_EQ: Regex = Regex::new(r"^>=").unwrap();
    // Separators
    static ref RE_SEP_POINT: Regex = Regex::new(r"^\.").unwrap();
    static ref RE_SEP_COLON: Regex = Regex::new(r"^:").unwrap();
    static ref RE_SEP_COMMA: Regex = Regex::new(r"^,").unwrap();
    // Comments
    static ref RE_COMMENT: Regex = Regex::new(r"^//.*").unwrap();
    // Whitespace
    // This used \s before, but this includes newlines, which we want to treat
    // separately to enable the lexer to count lines.
    static ref RE_WHITESPACE: Regex = Regex::new(r"^( |\t)+").unwrap();
    static ref RE_NEWLINE: Regex = Regex::new(r"^\r?\n").unwrap();
    // Empty regex for tokens that do not need one.
    static ref RE_EMPTY: Regex = Regex::new(r"").unwrap();
}

#[derive(Clone, PartialEq, Debug)]
pub struct Token {
    token_class: TokenClass,
    token_type: TokenType,
    line: u32,
    start: usize,
    end: usize,
    text: String, // TODO Maybe use &str
}

impl Token {
    pub fn new(token_type: TokenType, line: u32, start: usize, end: usize, value: &str) -> Self {
        Token {
            token_class: TokenClass::for_type(&token_type),
            token_type,
            line,
            start,
            end,
            text: value.to_owned(),
        }
    }

    fn none_token() -> Self {
        Token::new(TokenType::NotYetLexed, 0, 0, 0, "")
    }

    fn eof_token(line: u32) -> Self {
        Token::new(TokenType::EOF, line, 0, 0, "")
    }

    pub fn token_class(&self) -> TokenClass {
        self.token_class
    }

    pub fn token_type(&self) -> TokenType {
        self.token_type
    }

    pub fn line(&self) -> u32 {
        self.line
    }

    pub fn start(&self) -> usize {
        self.start
    }

    pub fn end(&self) -> usize {
        self.end
    }

    pub fn text(&self) -> &String {
        &self.text
    }
}

pub struct Lexer<'a> {
    input: &'a str,
    current_token: Token,
    current_line: u32,
    current_start: usize,
}

impl<'a> Lexer<'a> {
    pub fn from_string<S>(input: S) -> Lexer<'a>
    where
        S: Into<&'a str>,
    {
        Lexer {
            input: input.into(),
            current_token: Token::none_token(),
            current_line: 1,
            current_start: 0,
        }
    }

    pub fn current_token(&self) -> &Token {
        &self.current_token
    }

    pub fn current_line(&self) -> u32 {
        self.current_line
    }

    pub fn lex(&mut self) -> Result<&Token, LexerError> {
        if self.current_token.token_type == TokenType::EOF {
            // We are at the end of the input, just return.
            return Ok(&self.current_token);
        }
        if self.current_start >= self.input.len() {
            self.current_token = Token::eof_token(self.current_line);
            return Ok(&self.current_token);
        }
        for tt in TokenType::iter() {
            // Skip keywords, we need to handle them specially as a subtype of
            // the regex for the identifier token type.
            let clazz = TokenClass::for_type(&tt);
            if clazz == TokenClass::Keyword || clazz == TokenClass::NotApplicable {
                continue;
            }
            if let Some(m) = tt.regex().find(&self.input[self.current_start..]) {
                if TokenClass::for_type(&tt) == TokenClass::Ignorable {
                    if tt == TokenType::Newline {
                        self.current_line += 1;
                    }
                    self.update_start(&m);
                    return self.lex();
                }

                self.current_token = Self::make_token(tt, &m, self.current_line, self.current_start);

                self.update_start(&m);
                return Ok(&self.current_token);
            }
        }
        Err(LexerError::UnknownToken {
            offset: self.current_start,
        })
    }

    pub fn done(&self) -> bool {
        self.current_start >= self.input.len() || self.current_token.token_type == TokenType::EOF
    }

    fn update_start(&mut self, m: &regex::Match) {
        self.current_start += m.end();
    }

    fn make_token(tt: TokenType, m: &Match, line: u32, start: usize) -> Token {
        if tt == TokenType::Identifier {
            // Check if our identifier is actually a keyword.
            // We need to handle this in this way as otherwise, the regexes for
            // keywords could detect a partial identifier like repeater as keyword
            // ("repeat" in this case).
            // Alternatively, we could include some kind of separator into the keyword
            // regexes, but we would need to make sure to exclude it from the matches
            // end position. And that might be complicated, because the regex::Match::end
            // function returns not a character, but a byte position. And if the separating
            // character happens to be encoded in Unicode using more than one byte, we cannot
            // simply subtract 1 from end()'s return value and be sure to land at a valid
            // Unicode code point boundary.
            let mut concrete_type = tt;
            match m.as_str() {
                "action" => concrete_type = TokenType::KeywordAction,
                "if" => concrete_type = TokenType::KeywordIf,
                "else" => concrete_type = TokenType::KeywordElse,
                "elseif" => concrete_type = TokenType::KeywordElseif,
                "repeat" => concrete_type = TokenType::KeywordRepeat,
                "until" => concrete_type = TokenType::KeywordUntil,
                "while" => concrete_type = TokenType::KeywordWhile,
                "end" => concrete_type = TokenType::KeywordEnd,
                "type" => concrete_type = TokenType::KeywordType,
                "returns" => concrete_type = TokenType::KeywordReturns,
                "return" => concrete_type = TokenType::KeywordReturn,
                "self" => concrete_type = TokenType::KeywordSelf,
                _ => {} // Actually an identifier!
            }
            return Token::new(concrete_type, line, start, start + m.end(), m.as_str());
        } else {
            return Token::new(tt, line, start, start + m.end(), m.as_str());
        }
    }
}

#[cfg(test)]
mod tests {
    mod regexes {

        mod literals {
            use crate::lexer;
            use regex::Regex;

            #[test]
            fn test_re_literal_integer() {
                let re = Regex::new(format!("{}$", lexer::RE_LIT_INT.as_str()).as_str()).unwrap();
                assert!(re.is_match(r"13"));
                assert!(re.is_match(r"17_547"));
                assert!(re.is_match(r"-42"));
                assert!(re.is_match(r"0b0100111"));
                assert!(re.is_match(r"0b0100_1011"));
                assert!(!re.is_match(r"0b0100211"));
                assert!(re.is_match(r"0xAF2bFe"));
                assert!(re.is_match(r"0xAF_2b_Fe"));
                assert!(!re.is_match(r"0xAF2bge"));
            }

            #[test]
            fn test_re_literal_text() {
                let re = Regex::new(format!("{}$", lexer::RE_LIT_TEXT.as_str()).as_str()).unwrap();
                assert!(re.is_match(r#""""#));
                assert!(re.is_match(r#""foo""#));
                assert!(re.is_match(r#""This text contains spaces""#), "Spaces");
                assert!(
                    re.is_match(r#""This text contains space   and tabs""#),
                    "Tabs and Spaces"
                );
                assert!(
                    re.is_match(
                        r#""This text contains spaces.   And tabs! And even punctuation!?""#
                    ),
                    "Tabs and Spaces and Punctuation"
                );
                assert!(re.is_match(r#"",;.:!?""#), "Punctuation");
                assert!(re.is_match(r#""_""#), "Underscore");
                assert!(re.is_match(r#""+-*/=""#), "Mathematic Operators");
                assert!(re.is_match(r#""$&*#'"\""#), "Assorted Symbols");
                assert!(re.is_match(r#""()[]{}<>""#), "Parenthesis");
                assert!(re.is_match(r#""– — ⸺""#), "Dashes");
                assert!(re.is_match(r#""öäüß""#), "German Umlaute and Eszet");
                assert!(re.is_match(r#""²³""#), "Superscript numbrs");
                // Russian
                assert!(re.is_match(r#""внимание внимание""#), "Russian/Cyrillic");
                // Chinese (simplified)
                assert!(re.is_match(r#""战争的艺术""#), "Chinese (Simplified)");
                // Chinese (traditional)
                assert!(re.is_match(r#""戰爭的藝術""#), "Chinese (Traditional)");
                // Japanese
                assert!(re.is_match(r#""昇る太陽の国""#), "Japanese");
                // Hebrew
                assert!(re.is_match(r#""זה מבחן""#), "Hebrew");
                // Scottish /Gaelic
                assert!(
                    re.is_match(r#""Is e deuchainn a th ’ann""#),
                    "Scottish/Gaelic"
                );
                // French
                assert!(re.is_match(r#""Le fromage et le vin, c'est la vie. Mais nous avons besoin de beaucoup plus d'accents étranges.""#), "French");
                // Mongolian
                assert!(
                    re.is_match(r#""Хар аянга халуун ногоотой байв.""#),
                    "Mongolian"
                );
                // Arabic
                assert!(re.is_match(r#""بيت المعرفة في بغداد.""#), "Arabic");
            }
        }
    }

    mod lexer {
        use crate::{
            error::LexerError,
            lexer::{Lexer, TokenClass, TokenType},
        };

        #[test]
        fn test_lexer_creation() {
            // After creation, the lexer should have a special type of token
            // set as current token. This exists to tell a user that the lexing
            // process has not yet begun.
            let l = Lexer::from_string("");
            assert_eq!(TokenClass::NotApplicable, l.current_token.token_class());
            assert_eq!(TokenType::NotYetLexed, l.current_token.token_type());
            assert_eq!(0, l.current_token().line());
            assert_eq!(0, l.current_token().start());
            assert_eq!(0, l.current_token().end());
        }

        #[test]
        fn test_lex_global() -> Result<(), LexerError> {
            let mut l = Lexer::from_string("number zero = 0");
            while !l.done() {
                l.lex()?;
                println!("Current token: {:?}", l.current_token());
            }
            Ok(())
        }

        #[test]
        fn test_lex_int_literals() -> Result<(), LexerError> {
            let mut l = Lexer::from_string("0b0010_0111 0xDEAD_BEEF 0x1cf3A90a 846 107_937");
            let mut i = 0;
            while !l.done() {
                if let Ok(token) = l.lex() {
                    if token.token_type() == TokenType::Whitespace {
                        continue;
                    }
                    assert_eq!(
                        TokenType::Integral,
                        token.token_type(),
                        "Found token of wrong type: {:?}",
                        token.token_type()
                    );
                    i += 1;
                } else {
                    return Err(LexerError::UnknownToken {
                        offset: l.current_start,
                    });
                }
            }
            assert_eq!(5, i, "Wrong number of tokens found!");
            Ok(())
        }

        #[test]
        fn test_lex_text_literals() -> Result<(), LexerError> {
            let mut l = Lexer::from_string(
                r##"
                ""
                "SimpleText"
                "Text with spaces"
                "Text with punctuation and special characters äÜß$/%& 的艺"
                "##,
            );
            while !l.done() {
                let token = l.lex()?;
                if token.token_type() == TokenType::EOF {
                    continue;
                }
                println!("{:?}", token);
                assert_eq!(
                    TokenClass::Literal,
                    token.token_class(),
                    "Failed token class on line {}",
                    token.line()
                );
                assert_eq!(
                    TokenType::Text,
                    token.token_type(),
                    "Failed token type on line {}",
                    token.line()
                );
            }
            Ok(())
        }

        #[test]
        fn test_lex_newlines() -> Result<(), LexerError> {
            let mut l =
                Lexer::from_string("0b0010_0111 0xDEAD_BEEF\n 0x1cf3A90a 846\r\n 107_937\n");
            let mut i = 0;
            while !l.done() {
                let token = l.lex()?;
                i += 1;
                if i == 3 {
                    assert_eq!(2, token.line(), "Token found at wrong line");
                }
            }
            assert_eq!(4, l.current_line);
            Ok(())
        }

        #[test]
        fn test_lex_program_follow_line() -> Result<(), LexerError> {
            let mut l = Lexer::from_string(include_str!("../examples/simplex/follow-line.slx"));
            while !l.done() {
                let token = l.lex()?;
                println!("Current token: {:?}", token);
            }
            Ok(())
        }

        #[test]
        fn test_lex_program_lcd_print() -> Result<(), LexerError> {
            let mut l =
                Lexer::from_string(include_str!("../examples/simplex/basics/lcd-print.slx"));
            while !l.done() {
                let token = l.lex()?;
                println!("Current token: {:?}", token);
            }
            Ok(())
        }

        #[test]
        fn test_lex_program_cached_fields() -> Result<(), LexerError> {
            let mut l = Lexer::from_string(include_str!(
                "../examples/simplex/basics/custom-types/cached-fields.slx"
            ));
            while !l.done() {
                let token = l.lex()?;
                println!("Current token: {:?}", token);
            }
            Ok(())
        }

        #[test]
        fn test_lex_program_type_defaults() -> Result<(), LexerError> {
            let mut l = Lexer::from_string(include_str!(
                "../examples/simplex/basics/custom-types/type-default-value.slx"
            ));
            while !l.done() {
                let token = l.lex()?;
                println!("Current token: {:?}", token);
            }
            Ok(())
        }
    }
}
