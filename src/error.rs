#[derive(Debug)]
pub enum SimplexError {
    LexerError { cause: LexerError },
}

impl From<LexerError> for SimplexError {
    fn from(error: LexerError) -> Self {
        SimplexError::LexerError { cause: error }
    }
}

#[derive(Debug)]
pub enum LexerError {
    UnexpectedToken { expected: String, found: String },
    UnknownToken { offset: usize },
    OutOfInput,
}
