#[macro_use]
extern crate lazy_static;

pub mod error;
pub mod lexer;
pub mod parser;