# Simplex for Web
Simplex for Web is a re-implementation of the Simplex Language for WebAssembly.

**This is an extreme work in progress. Features advertised will most likely not yet work.**

## Simplex
Simplex is a language targeting the Lego Mindstorms EV3 platform. It is meant as a simple, easy to learn language for school students to transition from the graphical EV3 language to a text-based programming environment.

Simplex offers integral and floating point numbers as well as text strings as datatypes. As part of Simplex for Web, this is extended by adding arrays and structured custom types (`struct` in C, `record` in Pascal).

### Simplex Code Examples

Basic flow control:
```
integer x = 3
integer y = 4

if (x < y)
    // Play the tone 2500 for 2 seconds with volume 70.
    EV3.Sound:Tone(70, 2500, 2000)
else
    // Play the tone 5000 for 2 seconds with volume 70.
    EV3.Sound:Tone(70, 5000, 2000)
end
```

Print to LCD:
```
integer i = 0

repeat until i > 10000
    EV3.LCD:Clear()
    EV3.LCD:Text(10, 10, "i = " ++ EV3.Convert:IntegerToText(i, 5))
    EV3.LCD:Update()
    
    i = i + 1
end
```

Follow a black line on white background:
```
EV3.Sensor:SetMode(2, 2) // Color mode for port 2.

number color = 0
integer powerLeft = 0
integer powerRight = 0

repeat while true
    color = EV3.Sensor:SI(2) // Read the color.
    if (color = 1.0)
        // Color = 1 −> Black
        powerLeft = 60
        powerRight = 0
    elseif (color = 6.0)
        // Color = 6 −> White
        powerLeft = 0
        powerRight = 60
    else
        powerLeft = 50
        powerRight = 50
    end

    // Set motor ports to specific power and start them.
    EV3.Movement:Power("A", powerRight)
    EV3.Movement:Power("D", powerLeft)
    EV3.Movement:Start("A")
    EV3.Movement:Start("D")
end
```

More examples can be found in the [examples/simplex folder](https://gitlab.com/snkaupe/simplex/-/tree/master/examples/simplex).