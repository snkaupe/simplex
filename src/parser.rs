use crate::lexer::{Lexer, Token, TokenType};

pub enum ScopeBehaviour {
    Opening,
    Closing,
    ClosingAndOpening,
    NotApplicable,
}

impl ScopeBehaviour {
    fn for_token(token: &Token) -> ScopeBehaviour {
        Self::for_token_type(token.token_type())
    }

    fn for_token_type(tt: TokenType) -> ScopeBehaviour {
        match tt {
            TokenType::KeywordAction => ScopeBehaviour::Opening,
            TokenType::KeywordIf => ScopeBehaviour::Opening,
            TokenType::KeywordRepeat => ScopeBehaviour::Opening,
            TokenType::KeywordType => ScopeBehaviour::Opening,
            TokenType::KeywordElse => ScopeBehaviour::ClosingAndOpening,
            TokenType::KeywordElseif => ScopeBehaviour::ClosingAndOpening,
            TokenType::KeywordEnd => ScopeBehaviour::Closing,
            _ => ScopeBehaviour::NotApplicable,
        }
    }
}

pub struct Parser<'a> {
    lexer: Lexer<'a>,
    current_level: u32,
}

impl<'a> Parser<'a> {
    pub fn from_string<S>(input: S) -> Parser<'a>
    where
        S: Into<&'a str>,
    {
        Parser {
            lexer: Lexer::from_string(input),
            current_level: 0,
        }
    }
}
